use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum Command<'a> {
    GetPathes,
    SayHello { name: &'a str },
}
