use std::io::Write;

use anyhow::{Context, Result};
use interprocess::local_socket::LocalSocketStream;
use log::info;

use crate::{commands::Command, parse_args::OpenArgs, parse_configs::rices_config::RicesConfig};

pub fn open(args: &OpenArgs) -> Result<()> {
    let rices_config = RicesConfig::from_by_config_path(args.config_path.clone())
        .context("can't get rices config")?;
    let mut tmp = rices_config.pathes.tmp.clone();
    tmp.push("ricesd");

    let mut stream = LocalSocketStream::connect(tmp.with_extension("sock"))?;

    let command = bincode::serialize(&Command::SayHello { name: "Aboba" })?;
    let size = stream.write(&command)?;
    info!("write command with buffer of size: {size}");

    Ok(())
}
