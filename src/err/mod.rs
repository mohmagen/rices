use std::error::Error;

use colored::*;
use log::error;

use crate::parse_configs::get_config::ReadError;


pub struct Err {
    pub content: String, 
    pub source: String,
}


impl<T: std::string::ToString> From<T> for Err {

    fn from(value: T) -> Self {
        error!("from[{}]", value.to_string().red());
        Self {
            content: value.to_string(),
            source: "error".to_string(),
        }
    }
}

impl From<crate::pathes::GlobalPathes> for Err {
    fn from(value: crate::pathes::GlobalPathes) -> Self {
        Self {
            content: format!("path has been specified before. Path value {value:?}"),
            source: "path".into()
        }
    }
}

impl std::fmt::Debug for Err {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error!("[{}]:\n\t{}", self.source.red(), self.content);
        write!(f, "[{}]:\n\t{}", self.source.red(), self.content) 
    }
}

impl From<ReadError> for Err {
    fn from(value: ReadError) -> crate::err::Err {
        Err {
            source: "config read".to_string(),
            content: format!(
                "can't open config with name <{}>. (expect at \"{}\").\n",
                value.name.bright_magenta(),
                value.path.magenta()
            ),
        }
    }
}

