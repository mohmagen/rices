pub mod err;
pub mod parse_args;
pub mod parse_configs;
pub mod pathes;
pub mod run;
pub mod open;
pub mod commands;

use clap::Parser;
use parse_args::Command::*;
use log::*;
use colored::Colorize;
use anyhow::{Result, Context};

fn main() -> Result<()> {
    simple_logger::SimpleLogger::new().env().init().unwrap();

    let args = parse_args::Rices::parse();

    let rices_msg: String = String::from(r"

┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃                                                    ┃
┃        ██████╗ ██╗ ██████╗███████╗███████╗         ┃
┃        ██╔══██╗██║██╔════╝██╔════╝██╔════╝         ┃
┃        ██████╔╝██║██║     █████╗  ███████╗         ┃
┃        ██╔══██╗██║██║     ██╔══╝  ╚════██║         ┃
┃        ██║  ██║██║╚██████╗███████╗███████║         ┃
┃        ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝╚══════╝         ┃
┃                                                    ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
    ");
    info!("{}", rices_msg.green());

    match args.command {
        Some(Run(run_args)) => {
            run::run(&run_args).context("failed to execute 'run' command")?;
        },
        Some(Open(open_args)) => {
            open::open(&open_args).context("failed to execute 'open' command")?;
        }
        None => println!("Nothing to do"),
    };


    Ok(())
}
