use std::fs::File;
use std::io::Read;

use crate::parse_configs::rice_config::RiceConfig;

use crate::pathes::{GlobalPathes, GLOBAL_PATHES};

pub fn by_name(name: String) -> Result<RiceConfig, crate::err::Err> {
    let Some(GlobalPathes { config_path: mut path, .. }) = GLOBAL_PATHES.get().cloned() else {
        return Err("Path variable hasn't seted yet.".into());
    };
    path.push(&name);
    path.push("config");
    path.set_extension("toml");

    match File::open(&path) {
        Ok(mut file) => {
            let mut content = String::new();
            file.read_to_string(&mut content)?;

            let conf: RiceConfig = toml::from_str(&content)?;
            Ok(conf)
        }
        Err(_) => Err(ReadError {
            name,
            path: path.to_str().unwrap_or_default().to_string(),
        }
        .into()),
    }
}

pub struct ReadError {
    pub name: String,
    pub path: String,
}

