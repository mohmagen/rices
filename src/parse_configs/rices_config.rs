use std::fs::read_to_string;
use std::path::PathBuf;
use std::sync::OnceLock;

use anyhow::{Context, Result};
use lazy_static::lazy_static;
use serde::Deserialize;
use std::env::var;

lazy_static! {
    static ref DEFAULT_TMP_PATH: PathBuf = PathBuf::from("/tmp");
}

impl RicesConfig {
    pub fn from_by_config_path(config_path: Option<PathBuf>) -> Result<Self> {
        let binding = PathBuf::from(var("HOME")? + "/.config/rices/");
        let config_path = config_path.unwrap_or(binding);

        let mut rices_config_path = config_path.clone();
        rices_config_path.push("config");
        rices_config_path.set_extension("toml");

        let mut config: RicesConfig = toml::from_str(
            read_to_string(&rices_config_path)
                .context(format!("can't read file {rices_config_path:?}"))?
                .as_str(),
        )
        .context("can't deserialize rices config from file string")?;

        config.pathes.config = config_path;
        Ok(config)
    }

    pub fn create_tmp_dir(&self) -> Result<()> {
        std::fs::create_dir_all(&self.pathes.tmp).context("can't create dir for tmp path")?;
        Ok(())
    }
}

pub static RICES_CONFIG: OnceLock<RicesConfig> = OnceLock::new();

#[derive(Deserialize, Debug, Clone)]
pub struct RicesConfig {
    pub pathes: Pathes,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Pathes {
    #[serde(default = "tmp_default")]
    pub tmp: PathBuf,

    #[serde(default = "config_default")]
    pub config: PathBuf,
}

fn tmp_default() -> PathBuf {
    PathBuf::from("/tmp")
}

fn config_default() -> PathBuf {
    PathBuf::from(var("HOME").unwrap() + "/.config/rices/")
}
