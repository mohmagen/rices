use serde::Deserialize;

use colored::*;
use toml::Table;

#[derive(Deserialize, Debug, Clone)]
pub struct RiceConfig {
    pub infa: Infa,
    pub setups: Option<Setups>,
    pub rice: Rice,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Rice {
    pub start: String,
    pub shut_down: String,
    pub start_before: Option<String>,
    pub start_after: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Setups {
    pub alias: Option<Table>,
    pub exports: Option<Table>,
    pub links: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Infa {
    pub full_name: String,
    pub short_name: String,
    pub description: Option<String>,
}

impl std::fmt::Display for RiceConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "\n┏{:^30}┓\n┃{:^30}┃\n┗{:^28}┛",
            "━".repeat(30),
            self.infa.full_name.blue(),
            "━".repeat(30)
        )?;
        writeln!(f, "\n {:^30} ", format!("[{}]", self.infa.short_name))?;
        if let Some(description) = self.infa.description.clone() {
            let description = clear_description(description);
            for line in textwrap::wrap(description.as_str(), 28).into_iter() {
                writeln!(f, " {:^30}", line.white())?;
            }
        }

        Ok(())
    }
}

fn clear_description(description: String) -> String {
    let description = description.replace(['\n', '\t'], " ").to_string();
    let repeat: regex::Regex = regex::Regex::new(
                r"\ {2,}"
    ).unwrap();
    repeat.replace_all(description.as_str(), " ").to_string()
}
