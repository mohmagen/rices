use std::path::PathBuf;

use clap::{Parser, Subcommand, Args};


/// RICE CONTROL SYSTEM.
#[derive(Debug, Parser, Clone)]
#[clap(name = "rires", version = "0.1.0", author = "Rbi ra")]
pub struct Rices {
    /// quite mode  
    #[clap(short, long)]
    pub quite: bool,
    
    #[command(subcommand)]
    pub command: Option<Command>,
}

#[derive(Subcommand, Debug, Clone)]
pub enum Command {
    /// Open new rice, will close previous.
    Open (OpenArgs),

    /// Run rices daemon
    Run (RunArgs),
}

#[derive(Args, Debug, Clone)]
pub struct OpenArgs {
    /// Rice name
    pub name: String,

    /// specify path, default "$HOME/.config/rices"
    #[clap(short, long)]
    pub config_path: Option<PathBuf>,
}

#[derive(Args, Debug, Clone)]
pub struct RunArgs {
    /// specify path, default "$HOME/.config/rices"
    #[clap(short, long)]
    pub config_path: Option<PathBuf>,
}
