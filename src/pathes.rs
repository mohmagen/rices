use std::{sync::OnceLock, path::PathBuf, env::var};

use crate::parse_configs::rices_config::RicesConfig;

#[derive(Clone, Debug)]
pub struct GlobalPathes {
    pub tmp_path: PathBuf,
    pub config_path: PathBuf,
}

pub static GLOBAL_PATHES: OnceLock<GlobalPathes> = OnceLock::new();


impl GlobalPathes {
    pub fn new(
        config_path: Option<PathBuf>,
        rices_config: RicesConfig,
    ) -> Result<GlobalPathes, crate::err::Err> {
        let default_config_path = PathBuf::from(var("HOME")? + ".config/rices");
        let default_tmp_path = PathBuf::from("/tmp/.config/rices");


        Ok(GlobalPathes {
            config_path: config_path.unwrap_or(default_config_path),
            tmp_path: rices_config.pathes.tmp,
        })
    }
}
