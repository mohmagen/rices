
use std::io::Read;

use log::{error, info};

use crate::parse_configs::rices_config::RicesConfig;
use crate::{err, parse_args::RunArgs};
use anyhow::Result;

pub mod daemonize;
pub mod start_socket;
pub mod main_loop;

pub fn run(args: &RunArgs) -> Result<()> {
    let rices_config = RicesConfig::from_by_config_path(args.config_path.clone())?;
    rices_config.create_tmp_dir()?;
    
    daemonize::start_daemonize(&rices_config)?;
    let socket = start_socket::start_socket(&rices_config)?;


    for stream in socket.incoming() {

        match stream {
            Ok(mut stream) => {
                if let Err(err) = main_loop::main_loop_iteration(stream, &rices_config) {
                    error!("{err:?}");
                }
            }
            Err(err) => {
                error!("{:?}", crate::err::Err::from(err))
            }
        }

    }
    Ok(())
}
