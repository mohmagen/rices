use std::io::Read;

use interprocess::local_socket::LocalSocketStream;
use log::info;

use crate::parse_configs::rices_config::RicesConfig;


pub fn main_loop_iteration(
    mut stream: LocalSocketStream,
    config: &RicesConfig,
) -> Result<(), crate::err::Err> {
    let mut command: [u8;1024] = [0;1024];

    let size = stream.read(&mut command)?;
    let command = bincode::serialize(&command[0..size])?;


    info!(
        "command {command:?}",
    );
    Ok(())
}
