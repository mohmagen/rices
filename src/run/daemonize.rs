use crate::parse_configs::rices_config::RicesConfig;
use anyhow::{Context, Result};
use daemonize::Daemonize;
use std::fs::File;

pub fn start_daemonize(config: &RicesConfig) -> Result<()> {
    let mut tmp = config.pathes.tmp.clone();
    tmp.push("ricesd");

    let stdout = File::create(tmp.with_extension("out"))
        .context("can't create stdout file for ricesd.out")?;
    let stderr = File::create(tmp.with_extension("err"))
        .context("can't create stderr file for ricesd.err")?;

    std::fs::remove_file(tmp.with_extension("pid")).context("can't remove pid file")?;
    File::create(tmp.with_extension("pid")).context("can't create pid file")?;

    Daemonize::new()
        .pid_file(tmp.with_extension("pid"))
        .working_directory(&config.pathes.tmp)
        .stderr(stderr)
        .stdout(stdout)
        .start()
        .context("failed to start daemon for ricesd")?;
    Ok(())
}
