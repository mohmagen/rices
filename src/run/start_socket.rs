use interprocess::local_socket::{LocalSocketListener, ToLocalSocketName};

use crate::parse_configs::rices_config::RicesConfig;
use anyhow::{Context, Result};

pub fn start_socket(config: &RicesConfig) -> Result<LocalSocketListener> {
    let mut tmp = config.pathes.tmp.clone();
    tmp.push("ricesd");

    let name = tmp
        .with_extension("sock")
        .to_local_socket_name()
        .context("can't listener socket")?;

    let socket =
        LocalSocketListener::bind(name.into_inner()).context("can't bind listener socket")?;
    Ok(socket)
}
