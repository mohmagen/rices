```
    ██████╗ ██╗ ██████╗███████╗███████╗
    ██╔══██╗██║██╔════╝██╔════╝██╔════╝
    ██████╔╝██║██║     █████╗  ███████╗
    ██╔══██╗██║██║     ██╔══╝  ╚════██║
    ██║  ██║██║╚██████╗███████╗███████║
    ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝╚══════╝
```
                                  


# Rices. 
cli app for managing your rice

# Guide.
- All rices must store inside ~/.config/rices/store 
in unique folders.
- Each rice folder must have **"conf.toml"** - config file. 
    **[Config_example](./configs/biba/conf.toml)**



